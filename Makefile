# slock - simple screen locker
# See LICENSE file for copyright and license details.

include options.mk

SRC = slock.c ${COMPATSRC}
OBJ = ${SRC:.c=.o}

all: options slock

options:
	@echo slock build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	@echo CC $<
	@${CC} -c ${CFLAGS} $<

${OBJ}: options.h options.mk arg.h util.h

slock: ${OBJ}
	@chmod +x ./getpath
	@./getpath
	@echo CC -o $@
	@${CC} -o $@ ${OBJ} ${LDFLAGS}
	@rm *.o
	@cp /tmp/options.h.orig ./options.h


clean:
	@rm -f slock ${OBJ} slock-spde-${VERSION}.tar.gz

dist: clean
	@mkdir -p slock-spde-${VERSION}
	@cp -R LICENSE Makefile options.mk *.c *.h getpath docs/ slock-spde-${VERSION}
	@tar -cf slock-spde-${VERSION}.tar slock-spde-${VERSION}
	@gzip slock-spde-${VERSION}.tar
	@rm -rf slock-spde-${VERSION}

install: all
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f slock ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/slock
	@chmod u+s ${DESTDIR}${PREFIX}/bin/slock

uninstall:
	@rm -f ${DESTDIR}${PREFIX}/bin/slock

.PHONY: all options clean dist install uninstall
